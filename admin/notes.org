Le projet scientifique ne doit pas excéder 4 pages (y compris la
bibliographie, les schémas et références) pour la pré-proposition et
20 pages (y compris la bibliographie,13 les schémas et références)
pour la proposition complète.

Les projets souhaitant bénéficier21 du label d’un ou plusieurs pôle(s)
de compétitivité doivent le déclarer lors de la première étape du
processus de sélection.
* preproposition
** contenu
Descriptif du projet 
La pré-proposition doit décrire le projet et apporter les éléments
nécessaires à son évaluation selon les deux critères d’évaluation
prédéfinis (cf. Tableau 4). Elle doit donc suivre le plan suivant :
*** Contexte, positionnement et objectif(s) de la pré-proposition  
(en lien avec le critère d’évaluation « Qualité et ambition
scientifique ») 
Description des objectifs et des hypothèses de
recherche ; positionnement du projet par rapport à ’état de l’art ;
présentation de la méthodologie utilisée pour atteindre les objectifs,
prise en compte de l’interdisciplinarité ou transdisciplinaire du
projet dans la méthodologie choisie (cf.  Annexe 4 pour une définition
de l’inter et transdisciplinarité); démonstration du caractère
novateur et / ou ambitieux du projet, de son originalité tant du point
de vue des objectifs que de la méthodologie ; 
*positionnement du projet par rapport aux enjeux de recherche de l’axe scientifique choisi.*
*** Partenariat  
(en lien avec le critère d’évaluation « Organisation et réalisation du projet ») 
S’il s’agit d’un projet collaboratif (PRC ou PRCE) : présentation du
coordinateur ou de la coordinatrice scientifique, de son expérience en
termes de coordination de projet et dans le domaine objet de la
pré-proposition, implication dans le projet. Présentation du
consortium, de l’implication de chaque partenaire dans l’atteinte des
objectifs et de leur complémentarité pour l’atteinte des objectifs
** instructions
Le descriptif du projet doit : 
*** Comporter un maximum de 4 pages. 
*** Utiliser une mise en page permettant une lecture confortable du document 
(page A4, Calibri 11 ou équivalent, interligne simple, marges 2 cm ou
plus, numérotation des pages ; pour les tableaux et figures, minimum
Calibri 9 ou équivalent).
*** Être au format PDF 
(généré à partir d’un logiciel de traitement de texte, non scanné) 
sans aucune protection.  
*** Être rédigé préférentiellement en anglais.  
L’évaluation pouvant être réalisée par des personnalités scientifiques
non francophones, l’ANR incite les coordinateurs et coordinatrices à
déposer les propositions en langue anglaise ou à fournir sur demande
la traduction en anglais du document initialement rédigé en
français. En cas d’impossibilité pour le coordinateur ou la
coordinatrice de fournir une traduction en anglais, celui-ci peut se
rapprocher de l’ANR afin de trouver une solution adaptée.
* domaine, axe
** Domaine « Sciences du numérique » 
** Axe  E.3 : Sciences  et  génie  du  logiciel  -  Réseaux  de  communication  multi-usages, infra-structures de hautes performances

[...]
- Les  infrastructures  de  communication  et  de  calcul  haute  performance  permettant  le 
développement de modèles de calcul, d’algorithmes et l’exploitation d’un parallélisme 
massif,  l’optimisation  et  la  gestion  dynamique  des  ressources  en  s’appuyant  sur  des 
propriétés et mesures quantitatives (performance, robustesse, mémoire, efficacité 
énergétique), les environnements  de  programmation et l’algorithmique pour l’exascale. 
Ce  domaine  cible  aussi  l’étude  de  la  répartition  de  calcul  sur  diverses  topologies  et 
architectures de réseaux (« edge cloud », fog, cloud, cache, etc.) connectant des 
ressources  de  calcul  ainsi  que  les  divers  aspects  de  virtualisation  d’applications,  de 
serveurs, de réseaux (SDN), etc. sans omettre les aspects de sécurité ; 
[...]

Mots-clés associés : systèmes d'exploitation, OS temps réel, intergiciels, ingénierie logicielle, 
virtualisation, systèmes auto-adaptatifs, supervision ; systèmes embarqués, objets connectés, architectures 
matérielles  hétérogènes,  sécurité  des  systèmes  matériels ;  prototypage  virtuel,  composition  de  services, 
programmation  et  sécurité  Web,  plates-formes  de  services ;  compilation  optimisée  vers  des  architectures 
centralisées ou parallèles (multi-coeurs), modèles de calcul pour le parallélisme, systèmes et algorithmique 
distribués,  langages  de  programmation,  technologie  blockchain ;  orchestration  de  protocoles  et  services, 
métrologie, architectures logicielles, analyse de programmes, identification et correction de vulnérabilités, 
lutte contre les logiciels malveillants,  vérification, preuve de propriétés de sûreté et de sécurité, méthodes 
de test et débogage ; communications optiques, communication radio, architecture et programmabilité des 
systèmes  de  communication,  sécurité,  fiabilité  et  disponibilité,  mobilité,  passage  à  l’échelle,  élasticité, 
efficacité énergétique, plans de contrôle, de gestion et d'information ; accélérateurs hardware, parallélisme 
massif, cloud, orchestration et optimisation des ressources de communication/exécution/stockage, 
assurance  de  QoS  et  SLA,  data  analytics  pour  l’optimisation  des  réseaux,  sécurité  de  bout  en  bout, 
protocoles cryptographiques, gestion des infrastructures partagées, services sensibles au contexte, 
interface service-infrastructure, solutions de confiance, protection des données personnelles. 
Codes ERC associés : PE06, PE07.  
ODD associés : 8 et 9. 

* eval
AAPG 2022 1.1 – 25 septembre 2021  20 
Grille d’évaluation des projets déposés à l’appel à projets générique 2022 
Etape 1 (évaluation des pré-propositions)30 
** Qualité et ambition scientifique  
Lors  de  l’évaluation  en  phase  1,  ce  critère  est  discriminant  (nécessité  d’obtenir une 
notation A pour accéder à l’étape 2). 
o Clarté des objectifs et des hypothèses de recherche 
o Caractère novateur, originalité et/ou ambition, positionnement par rapport à l’état de 
l’art 
o Pertinence de la méthodologie au regard des aspects disciplinaires, interdisciplinaires 
ou transdisciplinaires 
o Capacité du projet à répondre aux enjeux de recherche de l’axe scientifique choisi 
** Organisation et réalisation du projet 
o Compétence, expertise et implication du coordinateur ou de la coordinatrice 
scientifique  
o Pour les PRC/PRCE : Qualité et complémentarité du consortium, qualité de la 
collaboration 
o Pour les PRME : Qualité et expertise de l’équipe qui réalisera le projet  
o Pour les JCJC :  Apport  du  projet  à  la  prise  de  responsabilité  du  coordinateur  ou  de  la 
coordinatrice et au développement de son équipe
